﻿using UnityEngine;
using System.Collections;

public class testScript : MonoBehaviour
{
    [HideInInspector]
    public bool amIOnTopOfEveryone = false;
    public int myRingNumber = 0;
    public float smoothingWhenMoved = 0.06f;
    public float smoothingWhenFalling = 0.3f;

    private bool amIDragging = false;

    private bool limitXMovement = false;//Should we limit the x position to only the x position of the pole?
    private float xPositionOfPoles = 0;

    private RaycastHit2D hit;

    private float lowestYPositionAllowed;
    private DecideWhosOnTop theLastPoleIWasOn;

    private testScript latestPlayerHit;

    private Vector3 desiredPosition;
    private Vector3 velocity = Vector3.zero; //Used for smoothdamp

    private bool haveIBeenMovedByThePlayer = true; //I put this to true at start to make the rings have the correct position when starting

    private Vector3 currentlySelectedPosition = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        CheckIfICanMove();
    }

    void OnMouseDown()
    {
        amIDragging = true;

        //Stop all coroutines inside the gamemanager script so object doesn't continue to shake.
        GameManager.instance.StopAllCoroutines();

        //If a ring is not on a pole and that ring is not us. then shake the ring
        if (GameManager.instance.ringObject != null && GameManager.instance.ringObject != gameObject)
        {
            GameManager.instance.ShakeObject();
        }
    }

    void OnMouseUp()
    {
        CheckIfICanMove();

        amIDragging = false;

        //if we are not on a pole then add the ring to the gamemanager to make it not possible to grab 2 rings at once.

        if (!limitXMovement)//We are not on a pole so add ourselves onto the gamemanager instace
        {
            GameManager.instance.ringObject = gameObject;
        }
        //We are on a pole. check if the ringObject on GameManager is us. if it is make ringObject null.
        else if (GameManager.instance.ringObject == gameObject)
        {
            ResetWhoIsOnTop();
        }
        if (limitXMovement) 
        {
            //Check if the ring below us is a lower number than ourselves.
            //If it is then allow the move onto the pole. if not. Move this ring to 0,4,z 
            hit = Physics2D.Raycast(transform.position, Vector2.down, 10);
            if (hit)
            {
                var hitRingScript = hit.collider.GetComponent<testScript>();
                if (hitRingScript)//We hit a ring. check its number
                {
                    if (hitRingScript.myRingNumber < myRingNumber)
                    {
                        ResetWhoIsOnTop();
                    }
                    else//We are trying to place a big ring on a small ring. prevent it
                    {
                        limitXMovement = false;
                        GameManager.instance.ringObject = gameObject;
                        StartCoroutine(moveRingToDefaultPosition());
                    }
                }
                else //We hit something but it wasn't a ring
                {
                    ResetWhoIsOnTop();
                }
            }
            else//We hit nothing
            {
                ResetWhoIsOnTop();
            }
        }
    }

    void ResetWhoIsOnTop()
    {
        GameManager.instance.ringObject = null;

        //Call the decideWhosOnTopScript to update who is on top.
        if (theLastPoleIWasOn)
            GameManager.instance.UpdateWhosOnTop(theLastPoleIWasOn);
    }

    void CheckIfICanMove()
    {
        if (GameManager.instance.ringObject == null || GameManager.instance.ringObject == gameObject)
        {
            if (amIDragging && amIOnTopOfEveryone)
            {
                DragMe();
                haveIBeenMovedByThePlayer = true;
            }
            else if (haveIBeenMovedByThePlayer && !amIDragging && limitXMovement)
            {//We are on a pole. and we stopped dragging the object. lets move ourselves down to the lowest point allowed
                desiredPosition = new Vector3(xPositionOfPoles, lowestYPositionAllowed, transform.position.z);
                transform.position = Smoothing(desiredPosition, smoothingWhenFalling);

                if (Vector3.Distance(transform.position, desiredPosition) < 0.09f) //We are very close to position that we want to be on. So stop updating this position
                {
                    haveIBeenMovedByThePlayer = false;
                }
            }
        }
    }

    //Touched a pole
    void OnTriggerEnter2D(Collider2D other)
    {
        limitXMovement = true;

        xPositionOfPoles = other.transform.position.x;

        //We touched a pole. let's check what the lowest position we can go is.
        hit = Physics2D.Raycast(new Vector2(xPositionOfPoles, transform.position.y), Vector2.down, 20);
        if (hit)
        {
            lowestYPositionAllowed = hit.point.y;

            //Removes the bug that happens if you move the object from a pole side straight in(what would happen without this is that wherever i moved the object to the side that is where its position would be)
            //Meaning we would be able to move several rings and the placement would be off.
            if (amIOnTopOfEveryone)
            {
                gameObject.layer = 2; //Put us into the ignore raycast layer
                hit = Physics2D.Raycast(new Vector2(xPositionOfPoles, 5), Vector2.down, 20);
                if (hit)
                    lowestYPositionAllowed = hit.point.y;
                gameObject.layer = 0;

                theLastPoleIWasOn = other.GetComponent<DecideWhosOnTop>();
            }
        }
    }

    //We left a pole so don't limit x movement
    void OnTriggerExit2D()
    {
        limitXMovement = false;
    }

    void DragMe()
    {
        //Smooth the angle.
        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, 0, 0), 0.1f);


#if UNITY_STANDALONE || UNITY_EDITOR
        currentlySelectedPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#endif
#if UNITY_IOS

        //TODO TEST THIS CODE. sadly the unity editor doesn't want to play ball with unity remote 4 for me.
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Moved || 
                Input.GetTouch(0).phase == TouchPhase.Began ||
                Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                currentlySelectedPosition = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
            }
        }
#endif
            
        if (limitXMovement)
        {
            desiredPosition = new Vector3(xPositionOfPoles, currentlySelectedPosition.y, transform.position.z);

            //Check if are below the lowestYPositionAllowed  and if we are. set the position's y value to be the allowed position
            if (currentlySelectedPosition.y < lowestYPositionAllowed)
                desiredPosition.y = lowestYPositionAllowed;
        }
        else
        {
            desiredPosition = new Vector3(currentlySelectedPosition.x, currentlySelectedPosition.y, transform.position.z);
        }
        transform.position = Smoothing(desiredPosition);
    }


    Vector3 Smoothing(Vector3 endPosition)
    {
        return Vector3.SmoothDamp(transform.position, endPosition, ref velocity, smoothingWhenMoved);
    }
    //Used when the ring should fall down onto the lowest position
    Vector3 Smoothing(Vector3 endPosition, float speedOfSmoothing)
    {
        return Vector3.SmoothDamp(transform.position, endPosition, ref velocity, speedOfSmoothing);
    }

    IEnumerator moveRingToDefaultPosition()
    {
        var defaultPosition = new Vector3(0,4,transform.position.z);
        while (Vector3.Distance(transform.position,new Vector3(0,4,transform.position.z)) > 0.1f)
        {
            if (amIDragging)//If i started grabbing the object. stop this.
                break;
            transform.position = Smoothing(defaultPosition);
            yield return null;
        }
    }
}