﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SetCorrectAspectRatio : MonoBehaviour
{

    public Camera theCamera;
    void Awake()
    {
        //Our default camera option is ipad retina. the orthographic size for that is 6.1

        //If we are using an iphone 5 then use 5.1 as size for the camera
        if (UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5 ||
            UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5C ||
            UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5S)
        {
            theCamera.aspect = (float)1136/(float)640;
            theCamera.orthographicSize = 5.1f;
        }
    }
}
