﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;
    [HideInInspector]
    public GameObject ringObject;

    public float speedOfShake = 1;//How fast it shakes
    public float duration = 1;//How long it shakes
    void Awake()
    {
        instance = this;
    }

    public void ShakeObject()
    {
        StartCoroutine(ShakeIt(ringObject));
    }

    IEnumerator ShakeIt(GameObject GO)
    {
        float whenToStop = Time.time + duration;
        while (whenToStop > Time.time)
        {
            //Gotten from an answer on unityanswers.
            float angleAmount = (Mathf.Cos(Time.time * speedOfShake) * 180) / Mathf.PI * 0.5f;
            angleAmount = Mathf.Clamp(angleAmount, -15, 15);
            GO.transform.localRotation = Quaternion.Euler(0, 0, angleAmount);

            yield return null;
        }
    }

    public void UpdateWhosOnTop(DecideWhosOnTop pole)
    {
        pole.RemoveAndUpdateWhoIsOnTop();
    }
}
