﻿using UnityEngine;
using System.Collections;

public class DecideWhosOnTop : MonoBehaviour
{
    /// <summary>
    /// Decides which 1 of the rings we can move.
    /// </summary>

    public Collider2D coll2D;

    private RaycastHit2D hit;

    private testScript tester;

    void OnTriggerExit2D()
    {
        UpdateWhoIsOnTop();
    }

    void OnTriggerEnter2D()
    {
        RemoveAndUpdateWhoIsOnTop();
    }

    void UpdateWhoIsOnTop()
    {
        //Set the top ring to be able to move
        hit = Physics2D.Raycast(new Vector2(transform.position.x, transform.position.y + coll2D.bounds.extents.y), Vector2.down, 20);
        if (hit)
        {
            tester = hit.collider.GetComponent<testScript>();
            if (tester)
            {
                tester.amIOnTopOfEveryone = true;
            }
        }
    }

    public void RemoveAndUpdateWhoIsOnTop()
    {
        //Since we just joined this pole we should disable the last 1 we hit.
        if (tester)
            tester.amIOnTopOfEveryone = false;
        UpdateWhoIsOnTop();
    }
}
